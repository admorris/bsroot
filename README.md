# BSroot

A set of tools for block-bootstrapping using only ROOT as a hard dependency.
Inspired by the increased usage of ReDecay within LHCb.

## Dependencies

- ROOT 6.14.00 and above
  - `TH1` for making histograms
  - `RDataFrame` for quicker and easier ntuple manipulation
- (optional) Boost
  - `program_options` for command-line and config-file parsing
  - `timer` for the progress bar

## Example usage

```c++
std::string branch = "B0_M";
double xmin = 5000;
double xmax = 5500;
int nbins = 100;
int nsamples = 1000;
auto df = data_utils::read_DecayTreeTuple({"data.root"}, "DecayTreeTuple/DecayTree");
// Create a unique list of ReDecay block IDs
auto orig_IDs = data_utils::get_origIDs(df);
// Store bin contents of all histograms
content_matrix bin_contents;
for(int i{0}; i < nsamples; ++i)
{
	// Perform the resampling
	auto ps = stat_utils::resample(orig_IDs);
	auto jdf = data_utils::join_pseudosample(df, ps);
	// Bin the distributions
	simple_histogram hist(nbins, xmin, xmax);
	data_utils::fill_hist(jdf, branch, hist);
	bin_contents.emplace_back(hist.content);
}
// Make the histogram with bootstrapped errors
TH1D boot_hist((branch+"_bootstrapped").c_str(), "", nbins, xmin, xmax);
hist_utils::fill_bootstrap(boot_hist, bin_contents);
```

