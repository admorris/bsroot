#include <boost/algorithm/string/replace.hpp>
#include "data_utils.hpp"
#include "stat_utils.hpp"
#include <iostream>
namespace data_utils
{
	std::string pairfunc_expression(const std::string& a, const std::string& b)
	{
		std::string expression = "({a}+{b})*({a}+{b}+1) + 2*{b}";
		boost::replace_all(expression, "{a}", a);
		boost::replace_all(expression, "{b}", b);
		return expression;
	}
	dataframe read_DecayTreeTuple(const std::vector<std::string>& filenames, const std::string& tuplepath, const std::map<std::string, std::string>& definitions)
	{
		auto df = ROOT::RDataFrame(tuplepath, filenames)
			.Define(EVENT_ID,pairfunc_expression("eventNumber","runNumber"))
			.Define(BLOCK_ID,pairfunc_expression("RD_org_eventNumber","RD_org_runNumber"));
		for_each(definitions.begin(), definitions.end(), [&](const std::pair<std::string, std::string>& definition){
			std::cout << definition.first << " = " << definition.second << "\n";
			df = df.Define(definition.first, definition.second);
		});
		return df;
	}
	dataframe join_pseudosample(dataframe& df, pseudosample& ps, const std::string& ID_name)
	{
		return df.Define(SAMPLE_COUNT, [&](evtID_t ID){return ps[ID];}, {ID_name});
	}
	std::vector<evtID_t> get_origIDs(dataframe& df, const std::string& ID_name)
	{
		auto orig_IDs = *df.Take<evtID_t>(ID_name);
		orig_IDs.resize(std::distance(orig_IDs.begin(), std::unique(orig_IDs.begin(), orig_IDs.end())));
		return orig_IDs;
	}
	void fill_hist(dataframe& df, const std::string& branch, simple_histogram& hist)
	{
		df.Foreach([&](double d, sample_count_t w){hist.fill(d, w);}, {branch, SAMPLE_COUNT});
	}
}

