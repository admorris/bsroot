#include "simple_histogram.hpp"
simple_histogram::simple_histogram(size_t nbins, double xmin, double xmax)
{
	double binsize = (xmax - xmin) / nbins;
	for(size_t i = 0; i <= nbins; ++i)
	{
		edges.push_back(xmin + i*binsize);
	}
	content = column(nbins, 0);
}
simple_histogram::simple_histogram(std::vector<double> bin_edges) : edges(bin_edges)
{
	content = column(edges.size() - 1, 0);
}
void simple_histogram::fill(double entry, double weight)
{
	if(entry < edges[0] || entry > edges.back()) return;
	for(size_t i = 0; i < content.size(); ++i)
	{
		if(entry >= edges[i] && entry < edges[i+1])
		{
			content[i] += weight;
			return;
		}
	}
}

