#include <algorithm>
#include <numeric>
#include <cmath>
#include <random>
#include "stat_utils.hpp"
namespace stat_utils
{
	double mean(const column& v)
	{
		return std::accumulate(v.begin(), v.end(), 0) / v.size();
	}
	double stddev(const column& v)
	{
		if(v.size() == 1) return 0;
		double sigma = 0.0;
		double m = mean(v);
		std::for_each(v.begin(), v.end(), [&](const double x) {
			sigma += std::pow((x-m),2);
		});
		return std::sqrt(sigma / (v.size()-1));
	}
	static std::random_device random_device;
	pseudosample resample(const std::vector<evtID_t>& ID_column)
	{
		auto length = ID_column.size();
		std::mt19937 engine{random_device()}; // Re-seed the random engine
		// Draw the size of this pseudosample from a Poisson distribution
		std::poisson_distribution<size_t> nboot(length);
		auto nsample = nboot(engine);
		if(nsample == 0) return {};
		// Do the resampling as a count of how many each event ID is sampled
		pseudosample times_sampled; // This will store the number of times each block is sampled
		std::uniform_int_distribution<size_t> dist(0, length - 1); // Uniform distribution from which to sample indices
		for(unsigned i = 0; i < nsample; ++i)
		{
			auto key = ID_column[dist(engine)];
			++times_sampled[key];
		}
		return times_sampled;
	}
}

