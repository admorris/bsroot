#include "hist_utils.hpp"
#include "stat_utils.hpp"

namespace hist_utils
{
	content_matrix transpose(const content_matrix& v)
	{
		if (v.size() == 0) return content_matrix();
		content_matrix vt(v[0].size(), column{});
		for(size_t i = 0; i < v.size(); i++)
		{
			for(size_t j = 0; j < v[i].size(); j++)
			{
				vt[j].resize(v.size());
				vt[j][i] = v[i][j];
			}
		}
		return vt;
	}
	void fill_bootstrap(TH1& hist, const content_matrix& bin_contents)
	{
		content_matrix transposed_bin_contents = transpose(bin_contents);
		for(int i = 0; i < hist.GetNbinsX(); ++i)
		{
			hist.SetBinContent(i+1, stat_utils::mean(transposed_bin_contents[i]));
			hist.SetBinError(i+1, stat_utils::stddev(transposed_bin_contents[i]));
		}
	}
}

