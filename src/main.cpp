#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

#include <TH1D.h>
#include <TCanvas.h>

#include <boost/program_options.hpp>
#include <boost/progress.hpp>

#include "stat_utils.hpp"
#include "hist_utils.hpp"
#include "data_utils.hpp"
#include "simple_histogram.cpp"

int main(int argc, char* argv[])
{
	std::string path;
	int nbins, nsamples;
	double xmin, xmax;
	namespace po = boost::program_options;
	po::options_description opts("Allowed options", 120);
	opts.add_options()
		("help", "produce help message")
		("path", po::value<std::string>(&path)->default_value("DecayTree"), "path to tuple")
		("bins", po::value<int>(&nbins)->default_value(100), "number of bins")
		("samples", po::value<int>(&nsamples)->default_value(1000), "number of pseudosamples")
		("upper", po::value<double>(&xmax), "upper x-axis limit")
		("lower", po::value<double>(&xmin), "lower x-axis limit")
		("auto", "automatic axis ranges, overrides --upper and --lower")
		("branch", po::value<std::string>(), "branch")
		("file", po::value<std::vector<std::string>>(), "input file(s)")
	;
	// TODO: multiple branches?
	// TODO: configurable branch type?
	po::positional_options_description post;
	post.add("branch", 1);
	post.add("file", -1);
	po::variables_map vmap;
	po::command_line_parser parser{argc, argv};
	parser.options(opts).positional(post);
	po::store(parser.run(), vmap);
	po::notify(vmap);
	if(vmap.count("help"))
	{
		std::cout << opts << std::endl;
		return 1;
	}
	std::vector<std::string> filenames = vmap["file"].as<std::vector<std::string>>();
	std::string branch = "tmp1";
	std::map<std::string, std::string> definitions = {
		{branch, vmap["branch"].as<std::string>()}
	};
	// Read the ntuple
	auto df = data_utils::read_DecayTreeTuple(filenames, path, definitions);
	if(vmap.count("auto"))
	{
		xmin = *df.Min(branch);
		xmax = *df.Max(branch);
		xmax += (xmax-xmin)/1000.; // Make the range 0.1% bigger to include the max value
	}
	// Create a unique list of ReDecay block IDs
	// This assumes the candidates in the same block are adjacent to one another
	auto orig_IDs = data_utils::get_origIDs(df);
	// Store bin contents of all histograms
	content_matrix bin_contents;
	std::cout << "Bootstrapping " << nsamples << " times...\n";
	boost::progress_display show_progress(nsamples);
	for(int i{0}; i < nsamples; ++i)
	{
		// Perform the resampling
		auto ps = stat_utils::resample(orig_IDs);
		auto jdf = data_utils::join_pseudosample(df, ps);
		// Bin the distributions
		simple_histogram hist(nbins, xmin, xmax);
		data_utils::fill_hist(jdf, branch, hist);
		bin_contents.emplace_back(hist.content);
		++show_progress;
	}
	TFile output((branch+".root").c_str(), "RECREATE");
	// Make the histogram with bootstrapped errors
	TH1D boot_hist((branch+"_bootstrapped").c_str(), "", nbins, xmin, xmax);
	hist_utils::fill_bootstrap(boot_hist, bin_contents);
	boot_hist.Write();
	// Draw a histogram with Poisson errors for comparison
	auto naive_hist = *df.Fill<Double_t>(TH1D((branch+"_naive").c_str(), "", nbins, xmin, xmax),{branch});
	naive_hist.Write();
	// Some info
	double sum_boot{0};
	double sum_naive{0};
	std::cout << "Sqrt(N)\tNaive\tBoot\tDiff\n";
	for(int i{1}; i <= nbins; ++i)
	{
		std::cout << std::fixed << std::setprecision(1) << std::setw(6);
		std::cout << std::sqrt(naive_hist.GetBinContent(i)) << "\t";
		std::cout << naive_hist.GetBinError(i) << "\t";
		std::cout << boot_hist.GetBinError(i) << "\t";
		std::cout << 100.*(boot_hist.GetBinError(i)-naive_hist.GetBinError(i))/naive_hist.GetBinError(i) << "%\n";
		sum_boot += boot_hist.GetBinContent(i);
		sum_naive += naive_hist.GetBinContent(i);
	}
	std::cout << std::setprecision(0);
	std::cout << "Sample size:            " << *df.Count() << "\n";
	std::cout << "Number of event IDs:    " << orig_IDs.size() << "\n";
	std::cout << "Sum of bins naive hist: " << sum_naive << "\n";
	std::cout << "Sum of bins boot hist:  " << sum_boot << "\n";
	output.Close();
	return 0;
}

