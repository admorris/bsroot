#pragma once
#include <vector>
#include "defs.hpp"
/// Quick and simple way to bin a distribution
class simple_histogram
{
	public:
		/** Constructor for fixed-width binning
		    @param nbins Number of bins
		    @param xmin Lower limit
		    @param xmax Upper limit
		*/
		simple_histogram(size_t nbins, double xmin, double xmax);
		/** Constructor allowing for variable-width binning
		    @param bin_edges List of bin edges
		*/
		simple_histogram(std::vector<double> bin_edges);
		/** Fill the histogram
		    @param entry The value of this entry
		    @param weight Optional weight
		*/
		void fill(double entry, double weight = 1);
		/// Container for the bin contents
		column content;
		/// A `nbins+1`-length list of bin edges
		std::vector<double> edges;
};

