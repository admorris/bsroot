#pragma once
#include <vector>
#include <string>
#include <map>
#include "simple_histogram.hpp"
#include "defs.hpp"
/// Useful functions for manipulating data
namespace data_utils
{
	/** Pair function creates a unique integer from two others.
	    This is useful if your block is identified by two numbers, e.g. run number and event number.
	    @param a The first variable name.
	    @param b The second variable name.
	   	@return The expression to be interpreted by RDataFrame or TF1.
	*/
	std::string pairfunc_expression(const std::string& a, const std::string& b);
	/** Create a ROOT dataframe from an ntuple made by DecayTreeTuple with MCTupleToolReDecay included.
	    @param filenames A list of filenames containing the ntuple.
	    @param tuplepath The path to the ntuple in the file(s).
	    @param definitions A std::map containing new branch names as keys and formulae as definitions. Can be left empty.
	    @return A ROOT dataframe containing the ntuple with extra unique ID branches for events and ReDecay blocks.
	*/
	dataframe read_DecayTreeTuple(const std::vector<std::string>& filenames, const std::string& tuplepath, const std::map<std::string, std::string>& definitions = {});
	/** Add the output of stat_utils::resample as a column to the dataframe
	    @param df The dataframe to modify
	    @param ps A std::map object that stores the number of times each block/event is sampled (output of stat_utils::resample)
	    @param ID_name The name of the unique block/event ID column
	    @return The dataframe with the extra column
	*/
	dataframe join_pseudosample(dataframe& df, pseudosample& ps, const std::string& ID_name = BLOCK_ID);
	/** Retrieve a list of unique event/block IDs from a dataframe
	    @param df The dataframe
	    @param ID_name The name of the unique block/event ID column
	    @return A std::vector of unique event/block IDs
	*/
	std::vector<evtID_t> get_origIDs(dataframe& df, const std::string& ID_name = BLOCK_ID);
	/** Bin the contents of a branch in a dataframe into a passed histogram
	    @param df The dataframe
	    @param branch The branch to use
	    @param hist The histogram to fill
	*/
	void fill_hist(dataframe& df, const std::string& branch, simple_histogram& hist);
}

