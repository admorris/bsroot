#pragma once
#include <vector>
#include <map>
#include "defs.hpp"
/// Useful functions for statistics
namespace stat_utils
{
	/// Take the mean value of a column of numbers.
	double mean(const column& v);
	/// Take the standard deviation of a column of numbers.
	double stddev(const column& v);
	/** Resample with replacement a list of IDs.
	    @param ID_column The list of IDs to be resampled.
	    @return A map where the keys are IDs and the values are the number of times sampled.
	*/
	std::map<evtID_t, sample_count_t> resample(const std::vector<evtID_t>& ID_column);
}

