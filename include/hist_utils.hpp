#pragma once
#include <TH1.h>
#include "defs.hpp"
/// Useful functions for manipulating histograms
namespace hist_utils
{
	/** Transpose a matrix of bin contents.
	    This makes it easier to take the mean and standard deviation of the same bin in many resampled distributions.
	    @param v The matrix to transpose.
	    @return The transposed matrix.
	*/
	content_matrix transpose(const content_matrix& v);
	/** Fill histogram bins using the means and standard deviations of the same bins in multiple other histograms.
	    @param hist The histogram to fill.
	    @param bin_contents The matrix of bin contents from the set of histograms.
	*/
	void fill_bootstrap(TH1& hist, const content_matrix& bin_contents);
}

