#pragma once
#include <ROOT/RDataFrame.hxx>
#include <vector>
#define EVENT_ID "event_ID"
#define BLOCK_ID "block_ID"
#define SAMPLE_COUNT "weight"
// The correct type for evtID_t is determined by the branches in the tuple
typedef unsigned long long evtID_t;
// sample_count_t is used for the number of times each block is sampled
// it should be big enough to hold values expected from a Poisson distribution with a mean of one and number of entries equal to the number of clusters
typedef unsigned char sample_count_t;
typedef ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager, void> dataframe;
typedef std::vector<double> column;
typedef std::vector<column> content_matrix;
typedef std::map<evtID_t, sample_count_t> pseudosample;

